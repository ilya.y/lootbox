package me.deft.misc;

/**
 * Created by iyakovlev on 09.10.17.
 */

public enum SortOrder {
    NAME_ASC, NAME_DESC, SIZE_ASC, SIZE_DESC
}
