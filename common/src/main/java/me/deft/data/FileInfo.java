package me.deft.data;

import java.io.File;
import java.io.Serializable;

/**
 * Created by iyakovlev on 19.09.17.
 */

public class FileInfo implements Serializable {

    private String mFileName;
    private FileType mType;
    private String mPath;
    private long mSize;

    public String getName() {
        return mFileName;
    }

    public FileType getType() {
        return mType;
    }

    public long getSize() {
        return mSize;
    }

    public String getPath() {
        return mPath;
    }

    public String getUserFilePath() {
        return getPath() + File.separator +  getName();
    }

    public void setFileName(String fileName) {
        mFileName = fileName;
    }

    public void setType(FileType type) {
        mType = type;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public void setSize(long size) {
        mSize = size;
    }

    public static class Builder {
        private String mFileName;
        private long mSize;
        private FileType mType;
        private String mPath;

        public Builder setFileName(String fileName) {
            mFileName = fileName;
            return this;
        }

        public Builder setSize(long size) {
            mSize = size;
            return this;
        }

        public Builder setType(FileType type) {
            mType = type;
            return this;
        }

        public Builder setPath(String path) {
            mPath = path;
            return this;
        }

        public FileInfo build() {
            FileInfo info = new FileInfo();


            if (mPath == null)
                throw new IllegalArgumentException("Incorrect file path");
            if (mFileName == null)
                throw new IllegalArgumentException("Incorrect file name");
            if (mType == null)
                throw new IllegalArgumentException("Incorrect file type");


            info.setFileName(mFileName);
            info.setPath(mPath);
            info.setType(mType);
            info.setSize(mSize);

            return info;
        }
    }
}
