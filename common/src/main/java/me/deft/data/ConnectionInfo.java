package me.deft.data;

import java.io.Serializable;

import me.deft.ServerConfig;

/**
 * Created by iyakovlev on 21.09.17.
 */

public class ConnectionInfo implements Serializable{

    private String mAddress;
    private int mPort;

    public ConnectionInfo(String address, int port) {
        mAddress = address;
        mPort = port;
    }

    public String getAddress() {
        return mAddress;
    }

    public int getPort() {
        return mPort;
    }
}