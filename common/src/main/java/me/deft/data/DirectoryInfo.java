package me.deft.data;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import me.deft.misc.SortOrder;

/**
 * Created by iyakovlev on 19.09.17.
 */

public class DirectoryInfo implements Serializable  {

    private String mDirectoryName;
    private String mPath;
    private List<FileInfo> mFiles;

    public String getName() {
        return mDirectoryName;
    }

    public String getPath() {
        return mPath;
    }

    public String getDirectoryName() {
        return mDirectoryName;
    }

    public List<FileInfo> getFiles() {
        return mFiles;
    }

    public void setDirectoryName(String directoryName) {
        mDirectoryName = directoryName;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public void setFiles(List<FileInfo> files) {
        mFiles = files;
    }

    public void sort(SortOrder order) {
        Comparator<FileInfo> comparator;

        switch (order) {
            case NAME_ASC: comparator = new NameAscendingComparator(); break;
            case NAME_DESC: comparator = new NameDescendingComparator(); break;
            default: comparator = new NameAscendingComparator();
        }
        mFiles = mFiles.stream().sorted(comparator).collect(Collectors.<FileInfo>toList());

    }

    private static class NameAscendingComparator implements Comparator<FileInfo> {
        @Override
        public int compare(FileInfo fileInfo, FileInfo t1) {
            return fileInfo.getName().compareTo(t1.getName());
        }
    }

    private static class NameDescendingComparator implements Comparator<FileInfo> {
        @Override
        public int compare(FileInfo fileInfo, FileInfo t1) {
            return -fileInfo.getName().compareTo(t1.getName());
        }
    }

}
