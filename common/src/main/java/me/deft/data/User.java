package me.deft.data;

import java.io.Serializable;

public class User implements Serializable{
    String mLogin;
    String mPassword;
    String mName;

    public User(String login, String password) {
        mLogin = login;
        mPassword = password;
    }

    public User(String login, String password, String name) {
        mLogin = login;
        mPassword = password;
        mName = name;
    }

    public String getLogin() {
        return mLogin;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getName() {
        return mName.isEmpty() ? mLogin : mName;
    }
}
