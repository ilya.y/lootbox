package me.deft.messages;

import me.deft.data.FileInfo;

/**
 * Created by iyakovlev on 21.09.17.
 */

public class FileDownloadRequest extends AbstractRequest<FileDownloadResponse> {

    private FileInfo mFileInfo;

    public FileDownloadRequest(FileInfo fileInfo) {
        mFileInfo = fileInfo;
    }

    public FileInfo getFileInfo() {
        return mFileInfo;
    }
}
