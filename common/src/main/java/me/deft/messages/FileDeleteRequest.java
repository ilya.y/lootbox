package me.deft.messages;

import me.deft.data.FileInfo;

/**
 * Created by iyakovlev on 15.10.2017.
 */

public class FileDeleteRequest extends AbstractRequest<FileDeleteResponse> {

    private FileInfo mFileInfo;

    public FileDeleteRequest(FileInfo fileInfo) {
        mFileInfo = fileInfo;
    }

    public FileInfo getFileInfo() {
        return mFileInfo;
    }
}
