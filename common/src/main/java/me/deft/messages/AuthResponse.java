package me.deft.messages;

import me.deft.data.User;

/**
 * Created by iyakovlev on 19.09.17.
 */

public class AuthResponse extends AbstractResponse<User> {

    public static final int ERROR_USER_NOT_FOUND = 1;
    public static final int ERROR_WRONG_PASSWORD = 2;

    public AuthResponse(Request request) {
        super(request);
    }

}
