package me.deft.messages;

import me.deft.data.FileInfo;

/**
 * Created by iyakovlev on 29.09.17.
 */

public class FileUploadRequest extends AbstractRequest<FileUploadResponse> {
    private FileInfo mFileInfo;

    public FileUploadRequest(FileInfo fileInfo) {
        mFileInfo = fileInfo;
    }

    public void setFileInfo(FileInfo fileInfo) {
        mFileInfo = fileInfo;
    }

    public FileInfo getFileInfo() {
        return mFileInfo;
    }
}
