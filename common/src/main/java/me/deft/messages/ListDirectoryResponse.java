package me.deft.messages;

import me.deft.data.DirectoryInfo;

/**
 * Created by iyakovlev on 20.09.17.
 */

public class ListDirectoryResponse extends AbstractResponse<DirectoryInfo> {
    public ListDirectoryResponse(Request request) {
        super(request);
    }
}
