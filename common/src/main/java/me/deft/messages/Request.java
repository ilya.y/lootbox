package me.deft.messages;

import java.io.Serializable;

public interface Request<R extends Response> extends Serializable{

    interface OnResponseListener<R> {
        void onSuccess(R response);
        void onError(Exception exception);
    }

    String getId();

    void setResultListener(OnResponseListener<R> listener);

}
