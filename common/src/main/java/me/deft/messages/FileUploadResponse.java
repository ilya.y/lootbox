package me.deft.messages;

import me.deft.data.ConnectionInfo;

/**
 * Created by iyakovlev on 29.09.17.
 */

public class FileUploadResponse extends AbstractResponse<ConnectionInfo> {

    public static final int ERROR_UPLOAD_FAILED = 1;

    public FileUploadResponse(Request request) {
        super(request);
    }
}
