package me.deft.messages;

/**
 * Created by iyakovlev on 15.10.2017.
 */

public class FileDeleteResponse extends AbstractResponse<Boolean> {

    public FileDeleteResponse(Request request) {
        super(request);
    }
}
