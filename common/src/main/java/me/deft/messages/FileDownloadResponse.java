package me.deft.messages;

import me.deft.data.ConnectionInfo;

/**
 * Created by iyakovlev on 21.09.17.
 */

public class FileDownloadResponse extends AbstractResponse<ConnectionInfo> {

    public static final int ERROR_UPLOAD_FAILED = 1;

    public FileDownloadResponse(Request request) {
        super(request);
    }
}
