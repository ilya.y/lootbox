package me.deft.messages;

/**
 * Created by iyakovlev on 17.10.17.
 */

public class MakeDirRequest extends AbstractRequest<MakeDirResponse> {

    private String mDirPath;

    public MakeDirRequest(String dirPath) {
        mDirPath = dirPath;
    }

    public String getDirPath() {
        return mDirPath;
    }
}
