package me.deft.messages;

/**
 * Created by iyakovlev on 17.10.17.
 */

public class MakeDirResponse extends AbstractResponse<Boolean> {

    public MakeDirResponse(Request request) {
        super(request);
    }
}
