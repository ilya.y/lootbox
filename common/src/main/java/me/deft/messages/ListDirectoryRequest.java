package me.deft.messages;

import me.deft.misc.SortOrder;

/**
 * Created by iyakovlev on 20.09.17.
 */

public class ListDirectoryRequest extends AbstractRequest<ListDirectoryResponse> {

    private String mPath;
    private SortOrder mSortOrder;

    public ListDirectoryRequest(String path, SortOrder order) {
        mPath = path;
        mSortOrder = order;
    }

    public String getPath() {
        return mPath;
    }

    public SortOrder getSortOrder() {
        return mSortOrder;
    }
}
