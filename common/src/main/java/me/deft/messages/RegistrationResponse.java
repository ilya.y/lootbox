package me.deft.messages;

import me.deft.data.User;

/**
 * Created by iyakovlev on 13.09.17.
 */

public class RegistrationResponse extends AbstractResponse<User> {

    public static final int ERROR_USER_EXIST = 1;
    public static final int ERROR_CANT_CREATE_USER = 2;


    public RegistrationResponse(Request request) {
        super(request);
    }


}


