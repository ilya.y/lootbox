package me.deft.messages;

/**
 * Created by iyakovlev on 13.09.17.
 */

public class RegistrationRequest extends AbstractRequest<RegistrationResponse> {

    private final String mLogin;
    private final String mPassword;
    private final String mName;

    public RegistrationRequest(String login, String password, String name) {
        mLogin = login;
        mPassword = password;
        mName = name;
    }

    public String getLogin() {
        return mLogin;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getName() {
        return mName;
    }
}
