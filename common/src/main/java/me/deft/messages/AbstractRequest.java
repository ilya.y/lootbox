package me.deft.messages;

import java.util.UUID;

public abstract class AbstractRequest<T extends Response> implements Request<T> {

    private final String ID;
    private OnResponseListener<T> mListener;

    public AbstractRequest() {
        this.ID = UUID.randomUUID().toString();
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setResultListener(OnResponseListener<T> listener) {
        mListener = listener;
    }
}
