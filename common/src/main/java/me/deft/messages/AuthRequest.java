package me.deft.messages;

/**
 * Created by iyakovlev on 19.09.17.
 */

public class AuthRequest extends AbstractRequest<AuthResponse> {

    private String mLogin;
    private String mPassword;

    public AuthRequest(String login, String password) {
        mLogin = login;
        mPassword = password;
    }

    public String getLogin() {
        return mLogin;
    }

    public String getPassword() {
        return mPassword;
    }
}

