package me.deft.messages;

import java.io.Serializable;

public abstract class AbstractResponse<T extends Serializable> implements Response<T> {
    private final String ID;
    private int mError = NO_ERROR;
    T mData;

    public AbstractResponse(Request request) {
        this.ID = request.getId();
    }


    @Override
    public final String getId() {
        return ID;
    }

    @Override
    public int getError() {
        return mError;
    }

    @Override
    public void setError(int error) {
        mError = error;
    }

    @Override
    public T getData() {
        return mData;
    }

    @Override
    public void setData(T data) {
        mData = data;
    }
}
