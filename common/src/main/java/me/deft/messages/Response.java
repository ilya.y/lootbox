package me.deft.messages;

import java.io.Serializable;

public interface Response<D> extends Serializable {

    int NO_ERROR = 0;
    int ERROR_GENERIC = 1000;
    int ERROR_IO = 1001;
    int ERROR_AUTH = 1002;

    String getId();

    D getData();

    void setData(D data);

    int getError();

    void setError(int error);
}
