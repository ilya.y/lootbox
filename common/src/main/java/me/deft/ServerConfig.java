package me.deft;

import java.io.File;
import java.net.InetAddress;

public class ServerConfig {
    public static int PORT = 2508;
    public static String ROOT_STORAGE = "storage/";
    public static String ADDRESS_LOCAL = "127.0.0.1";
    public static String ADDRESS_ANDROID_EMULATOR = "10.0.2.2";
    public static String ADDRESS_PUBLIC = ADDRESS_ANDROID_EMULATOR;



}
