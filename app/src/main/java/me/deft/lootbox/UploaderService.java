package me.deft.lootbox;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import me.deft.ServerConfig;

/**
 * Created by iyakovlev on 05.10.17.
 */

public class UploaderService extends IntentService {

    private static final String TAG = UploaderService.class.getSimpleName();

    private static final int SERVICE_ID = 5333;

    private static final String EXTRA_FILE_NAME = "extra_save_to";
    private static final String EXTRA_PORT = "extra_port";
    private static final String EXTRA_SIZE = "extra_size";
    private static final String EXTRA_FILE_PATH = "extra_uri";
    private static final int SOCKET_TIMEOUT = 30 * 1000;

    NotificationManager mNotificationManager;
    NotificationCompat.Builder mNotificationnBuilder;

    public static Intent getIntent(Context context, String filePath, String fileName, int port, long size) {
        Intent i = new Intent(context, UploaderService.class);
        i.putExtra(EXTRA_PORT, port);
        i.putExtra(EXTRA_FILE_PATH, filePath);
        i.putExtra(EXTRA_FILE_NAME, fileName);
        i.putExtra(EXTRA_SIZE, size);
        return i;
    }

    public UploaderService() {
        super(UploaderService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        final int port = intent.getIntExtra(EXTRA_PORT, -1);
        final String fileName = intent.getStringExtra(EXTRA_FILE_NAME);
        final String filePath = intent.getStringExtra(EXTRA_FILE_PATH);
        final long size = intent.getLongExtra(EXTRA_SIZE, 0);

        if (port == -1)
            return;

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationnBuilder = new NotificationCompat.Builder(this, UploaderService.class.getSimpleName());

        try(Socket socket = new Socket(ServerConfig.ADDRESS_PUBLIC, port);
            InputStream is = getContentResolver().openInputStream(Uri.parse(filePath));
            OutputStream os = new BufferedOutputStream(socket.getOutputStream())) {

            socket.setSoTimeout(SOCKET_TIMEOUT);

            int readCnt;
            long summ = 0;
            int completePercent = -1;
            byte[] buff = new byte[4096];
            while ((readCnt = is.read(buff)) != -1) {
                os.write(buff, 0, readCnt);
                os.flush();
                summ += readCnt;
                int currentProgress = (int) (summ * 100 / size);
                if (currentProgress > completePercent) {
                    completePercent = currentProgress;
                    Notification notification =
                            getProgressNotification(completePercent, fileName);
                    mNotificationManager.notify(SERVICE_ID, notification);
                }
            }
            mNotificationManager.notify(SERVICE_ID, getCompletedNotification(fileName));
            LocalBroadcastManager.getInstance(this).sendBroadcast(FileTransmissionBroadcastReceiver.getIntent(filePath));

        } catch (IOException e) {
            mNotificationManager.notify(SERVICE_ID, getErrorNotification(fileName));
        }
    }

    private Notification getProgressNotification(int progress, String fileName) {
        mNotificationnBuilder.setContentTitle(getString(R.string.uploader_service_uploading));
        mNotificationnBuilder.setProgress(100, progress, false);
        mNotificationnBuilder.setContentText(fileName);
        mNotificationnBuilder.setSmallIcon(R.drawable.ic_launcher_background);

        return mNotificationnBuilder.build();
    }

    private Notification getCompletedNotification(String fileName) {

        mNotificationnBuilder.setContentTitle(getString(R.string.uploader_service_upload_complete));
        mNotificationnBuilder.setContentText(fileName);
        mNotificationnBuilder.setProgress(0, 0, false);
        mNotificationnBuilder.setSmallIcon(R.drawable.ic_launcher_background);

        return mNotificationnBuilder.build();
    }

    private Notification getErrorNotification(String fileName) {

        mNotificationnBuilder.setContentTitle(getString(R.string.uploader_service_upload_error));
        mNotificationnBuilder.setContentText(fileName);
        mNotificationnBuilder.setProgress(0, 0, false);
        mNotificationnBuilder.setSmallIcon(R.drawable.ic_launcher_background);

        return mNotificationnBuilder.build();
    }

}
