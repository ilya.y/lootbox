package me.deft.lootbox;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

/**
 * Created by iyakovlev on 16.10.2017.
 */

public class FileTransmissionBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = FileTransmissionBroadcastReceiver.class.getSimpleName();

    public static final String ACTION = "me.deft.lootbox.broadcast.FILE_UPLOADED";
    public static final String EXTRA_PATH = "extra_path";

    private TransmissionListener mTransmissionListener;

    public interface TransmissionListener {
        void onFileUploaded(String path);
    }

    public static Intent getIntent(String path) {
        Intent intent = new Intent();
        intent.setAction(ACTION);
        intent.putExtra(EXTRA_PATH, path);
        return intent;
    }

    public static IntentFilter getIntentFilter(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION);
        return intentFilter;
    }

    public FileTransmissionBroadcastReceiver(TransmissionListener listener) {
        mTransmissionListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String path = intent.getExtras().getString(EXTRA_PATH);
        mTransmissionListener.onFileUploaded(path);
    }
}
