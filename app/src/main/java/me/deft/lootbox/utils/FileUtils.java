package me.deft.lootbox.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;

import java.io.File;

import me.deft.data.FileInfo;
import me.deft.data.FileType;

/**
 * Created by iyakovlev on 09.10.17.
 */

public class FileUtils {

    private Context mContext;

    public FileUtils(Context context) {
        mContext = context.getApplicationContext();
    }

    public  FileInfo getFileInfo(Uri uri) {
        FileInfo fileInfo = new FileInfo();

        Cursor fileData = mContext.getContentResolver().query(
                uri, null, null, null, null, null);

        fileData.moveToFirst();
        fileInfo.setType(mContext.getContentResolver().getType(uri).equals("text/directory") ? FileType.DIRECTORY : FileType.FILE);
        fileInfo.setFileName(fileData.getString(fileData.getColumnIndex(OpenableColumns.DISPLAY_NAME)));
        fileInfo.setSize(fileData.getLong(fileData.getColumnIndex(OpenableColumns.SIZE)));

        fileData.close();
        return fileInfo;
    }
}
