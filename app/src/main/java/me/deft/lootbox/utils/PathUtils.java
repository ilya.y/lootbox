package me.deft.lootbox.utils;

import java.io.File;

/**
 * Created by iyakovlev on 17.10.17.
 */

public class PathUtils {
    private static final String ROOT = File.separator;

    public static boolean isRoot(String path) {
        return path.equals(ROOT);
    }

    public static String up(String path) {
        if (!isRoot(path)) {
            int lastSlashIndex = path.lastIndexOf(File.separator);
            return path.substring(0, lastSlashIndex);
        } else {
            return path;
        }
    }

    public static String down(String currentPath, String folderName) {
        if (isRoot(currentPath)) {
            return currentPath + folderName;
        } else  {
            return currentPath + File.separator + folderName;
        }
    }
}
