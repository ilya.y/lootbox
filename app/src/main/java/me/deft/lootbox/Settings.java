package me.deft.lootbox;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import java.io.File;

/**
 * Created by iyakovlev on 28.09.17.
 */

public class Settings {

    private static final String APP_SETTINGS = "app_settings";

    private static final String SETTING_LOGIN = "login";
    private static final String SETTING_PASSWORD = "password";
    private static final String SETTING_LAST_PATH = "last_path";

    SharedPreferences mSharedPreferences;

    public Settings(Context context) {
        mSharedPreferences = context.getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE);
    }


    public void saveLogin(String login) {
        mSharedPreferences.edit().putString(SETTING_LOGIN, login).apply();
    }
    public void savePassword(String password) {
        mSharedPreferences.edit().putString(SETTING_PASSWORD, password).apply();
    }

    public void saveLastPath(String path) {
        mSharedPreferences.edit().putString(SETTING_LAST_PATH, path).apply();
    }

    public @Nullable String getlLogin() {
        return mSharedPreferences.getString(SETTING_LOGIN, null);
    }
    public @Nullable String getPassword() {
        return mSharedPreferences.getString(SETTING_PASSWORD, null);
    }

    public String getLastPath() {
        return mSharedPreferences.getString(SETTING_LAST_PATH, File.separator);
    }

    public void setUnauth() {
        savePassword(null);
        saveLogin(null);
    }



}
