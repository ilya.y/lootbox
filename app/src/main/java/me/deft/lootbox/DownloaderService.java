package me.deft.lootbox;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.Socket;

import me.deft.ServerConfig;

/**
 * Created by iyakovlev on 29.09.17.
 */

public class DownloaderService extends IntentService {

    private static final String TAG = DownloaderService.class.getSimpleName();

    private static final int SERVICE_ID = 5334;


    private static final String EXTRA_SAVE_TO = "extra_save_to";
    private static final String EXTRA_PORT = "extra_port";
    private static final String EXTRA_FILE_NAME = "extra_filename";
    private static final String EXTRA_SIZE = "extra_size";
    private static final int SOCKET_TIMEOUT = 30 * 1000;

    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;

    public static Intent getIntent(Context context, String fileName, String saveTo, long size, int port) {
        Intent i = new Intent(context, DownloaderService.class);
        i.putExtra(EXTRA_PORT, port);
        i.putExtra(EXTRA_SAVE_TO, saveTo);
        i.putExtra(EXTRA_FILE_NAME, fileName);
        i.putExtra(EXTRA_SIZE, size);
        return i;
    }

    public DownloaderService() {
        super(DownloaderService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        final int port = intent.getIntExtra(EXTRA_PORT, -1);
        final String saveTo = intent.getStringExtra(EXTRA_SAVE_TO);
        final String fileName = intent.getStringExtra(EXTRA_FILE_NAME);
        final long size = intent.getLongExtra(EXTRA_SIZE, 0);

        File fileToSave = new File(saveTo);

        if (port == -1)
            return;

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationBuilder = new NotificationCompat.Builder(this, DownloaderService.class.getSimpleName());
        try (Socket socket = new Socket(ServerConfig.ADDRESS_PUBLIC, port);
            InputStream is = new BufferedInputStream(socket.getInputStream());
            OutputStream os = new BufferedOutputStream(new FileOutputStream(fileToSave))) {

            socket.setSoTimeout(SOCKET_TIMEOUT);

            int readCnt;
            int summ = 0;
            int completePercent = -1;
            byte[] buff = new byte[4096];
            while ((readCnt = is.read(buff)) != -1) {
                os.write(buff, 0, readCnt);
                os.flush();

                summ += readCnt;
                int currentProgress = (int) (summ * 100 / size);
                if (currentProgress > completePercent) {
                    completePercent = currentProgress;
                    Notification notification =
                            getProgressNotification(completePercent, fileName);
                    mNotificationManager.notify(SERVICE_ID, notification);
                }
            }
            mNotificationManager.notify(SERVICE_ID, getCompletedNotification(fileName));

        } catch (IOException e) {
            fileToSave.delete();
            mNotificationManager.notify(SERVICE_ID, getErrorNotification(fileName));
        }
    }

    private Notification getProgressNotification(int progress, String fileName) {
        mNotificationBuilder.setContentTitle(getString(R.string.downloader_service_download_downloading));
        mNotificationBuilder.setProgress(100, progress, false);
        mNotificationBuilder.setContentText(fileName);
        mNotificationBuilder.setSmallIcon(R.drawable.ic_launcher_background);

        return mNotificationBuilder.build();
    }

    private Notification getCompletedNotification(String fileName) {

        mNotificationBuilder.setContentTitle(getString(R.string.downloader_service_download_complete));
        mNotificationBuilder.setContentText(fileName);
        mNotificationBuilder.setProgress(0, 0, false);
        mNotificationBuilder.setSmallIcon(R.drawable.ic_launcher_background);

        return mNotificationBuilder.build();
    }

    private Notification getErrorNotification(String fileName) {

        mNotificationBuilder.setContentTitle(getString(R.string.downloader_service_download_error));
        mNotificationBuilder.setContentText(fileName);
        mNotificationBuilder.setProgress(0, 0, false);
        mNotificationBuilder.setSmallIcon(R.drawable.ic_launcher_background);

        return mNotificationBuilder.build();
    }
}
