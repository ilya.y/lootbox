package me.deft.lootbox.network;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.deft.messages.Request;

/**
 * Created by iyakovlev on 13.10.17.
 * Класс-обвязка по взаимодействию с ConnectionService
 */

public class NetworkClient implements ServiceConnection, ConnectionService.ConnectionResultListener {

    private static final String TAG = NetworkClient.class.getSimpleName();
    private boolean mIsConnected;
    private boolean mIsConnecting;

    public interface ConnectionStatusListener {
        void onConnected();
        void onConnecting();
        void onDisconnected();
    }

    private List<ConnectionStatusListener> mConnectionStatusListeners = new LinkedList<>();
    // сюда мы складываем запросы, которые отправляются до того, как мы установили соединение с сервисом
    private Map<Request, ConnectionService.RequestListener> mPendingRequests = new HashMap<>();

    public void addClientStatusListener(ConnectionStatusListener listener, boolean notifyOnAdd) {
        mConnectionStatusListeners.add(listener);
        if (notifyOnAdd) {
            if (isConnected()) {
                listener.onConnected();
            } else if (isConnecting()){
                listener.onConnecting();
            } else {
                listener.onDisconnected();
            }
        }
    }

    public void removeClientStatusListener(ConnectionStatusListener listener) {
        mConnectionStatusListeners.remove(listener);
    }

    private void sendPendingRequests() {
        Set<Request> pendingRequests = mPendingRequests.keySet();

        for (Request r : pendingRequests){
            mConnectionService.enqueue(r, mPendingRequests.get(r));
        }
    }

    public void notifyConnected() {
        for (ConnectionStatusListener listener : mConnectionStatusListeners) {
            listener.onConnected();
        }
    }

    public void notifyConnecting() {
        for (ConnectionStatusListener listener : mConnectionStatusListeners) {
            listener.onConnecting();
        }
    }

    public void notifyDisconnected() {
        for (ConnectionStatusListener listener : mConnectionStatusListeners) {
            listener.onDisconnected();
        }
    }

    private ConnectionService mConnectionService;

    public boolean isConnected() {
        return mIsConnected;
    }

    public boolean isConnecting() {
        return mIsConnecting;
    }

    public void connect(Context context) {
        mIsConnecting = true;
        notifyConnecting();
        context.bindService(ConnectionService.getIntent(context),
                this,
                Context.BIND_AUTO_CREATE );
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        mIsConnecting = false;
        mConnectionService = ((ConnectionService.ConnectionBinder)iBinder).getConnectionService();
        mConnectionService.startConnection(this);


    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        mConnectionService = null;
        mIsConnecting = false;
        notifyDisconnected();
    }

    protected final void enqueue(Request request, ConnectionService.RequestListener listener) {
       if (mConnectionService == null) {
           mPendingRequests.put(request, listener);
       } else {
           mConnectionService.enqueue(request, listener);
       }
    }

    /**
     * Возвращает Context в случае, если мы уже прибайндились к сервису; в противном случае null.
     * @return
     */
    protected @Nullable Context getServiceContext() {
        return mConnectionService;
    }

    @Override
    public void onConnect() {
        mIsConnected = true;
        mIsConnecting = false;
        sendPendingRequests();
        notifyConnected();
    }

    @Override
    public void onError() {
        mIsConnected = false;
        mIsConnecting = false;
        Log.d(TAG, "Connection error");
        notifyDisconnected();
    }
}
