package me.deft.lootbox.network;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import me.deft.ServerConfig;
import me.deft.messages.Request;
import me.deft.messages.Response;

/**
 * Created by iyakovlev on 10.10.17.
 */

public class ConnectionService extends Service {
    private static final String TAG = ConnectionService.class.getSimpleName();

    private Socket mSocket;
    private ObjectInputStream mInputStream;
    private ObjectOutputStream mOutputStream;
    private Handler mMainThreadHandler;

    private Map<String, RequestListener> mRequests = new HashMap<>();
    private ExecutorService mClientExecutor = Executors.newFixedThreadPool(2);

    private boolean mIsConnected = false;

    public interface RequestListener<R extends Response> {
        void onError(int errorCode);
        void onResult(R result);
    }

    public interface ConnectionResultListener {
        void onConnect();
        void onError();
    }

    public static final Intent getIntent(Context context) {
        Intent intent = new Intent(context, ConnectionService.class);
        return intent;
    }

    public <R extends Response, T extends Request<R>> void enqueue(final T request, final RequestListener<R> listener) {
        sendToServer(request, listener);
    }

    public void startConnection(final ConnectionResultListener listener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mSocket = new Socket(ServerConfig.ADDRESS_PUBLIC, ServerConfig.PORT);
                    mInputStream = new ObjectInputStream(new BufferedInputStream(mSocket.getInputStream()));
                    mOutputStream = new ObjectOutputStream(new BufferedOutputStream(mSocket.getOutputStream()));
                    mIsConnected = true;
                    listener.onConnect();
                } catch (IOException e) {
                    e.printStackTrace();
                    listener.onError();
                    mIsConnected = false;
                }

                mClientExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        while (mIsConnected) {
                            try {
                                Object o = mInputStream.readObject();
                                if (o instanceof Response) {
                                    responseToClient((Response) o);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                mIsConnected = false;
                                listener.onError();
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                                listener.onError();
                            }
                        }
                    }
                });
            }
        }).start();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        mMainThreadHandler = new Handler();
        return new ConnectionBinder(this);
    }


    @Override
    public boolean onUnbind(Intent intent) {
        mIsConnected = false;
        mClientExecutor.shutdown();
        return super.onUnbind(intent);
    }

    private void sendToServer(final Request request, RequestListener listener) {
        mRequests.put(request.getId(), listener);
        if (mIsConnected) {
            mClientExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "run: executor start");
                    try {
                        mOutputStream.writeObject(request);
                        mOutputStream.flush();
                    } catch (IOException e) {
                        mRequests.get(request.getId()).onError(Response.ERROR_IO);
                    } finally {
                        Log.d(TAG, "run: executor end");
                    }
                }
            });
        }
    }

    private void responseToClient(final Response response) {
        Log.d(TAG, "responseToClient: " + response.getClass().getSimpleName());
        mMainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                RequestListener listener = mRequests.get(response.getId());
                if (listener == null) {
                    Log.d(TAG,"Incoming response for unknown request");
                    return;
                }
                mRequests.remove(response.getId());
                if (response.getError() == Response.NO_ERROR) {
                    listener.onResult(response);
                } else {
                    listener.onError(response.getError());
                }
            }
        });
    }


    public static class ConnectionBinder extends Binder {

        private final ConnectionService mConnectionService;

        public ConnectionBinder(ConnectionService connectionService) {
            mConnectionService = connectionService;
        }

        public ConnectionService getConnectionService() {
            return mConnectionService;
        }
    }

}
