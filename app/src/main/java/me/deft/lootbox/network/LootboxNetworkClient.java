package me.deft.lootbox.network;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import java.io.File;

import me.deft.data.FileInfo;
import me.deft.lootbox.DownloaderService;
import me.deft.lootbox.UploaderService;
import me.deft.messages.AuthRequest;
import me.deft.messages.AuthResponse;
import me.deft.messages.FileDeleteRequest;
import me.deft.messages.FileDeleteResponse;
import me.deft.messages.FileDownloadRequest;
import me.deft.messages.FileDownloadResponse;
import me.deft.messages.FileUploadRequest;
import me.deft.messages.FileUploadResponse;
import me.deft.messages.ListDirectoryRequest;
import me.deft.messages.ListDirectoryResponse;
import me.deft.messages.MakeDirRequest;
import me.deft.messages.MakeDirResponse;
import me.deft.messages.RegistrationRequest;
import me.deft.messages.RegistrationResponse;
import me.deft.misc.SortOrder;

/**
 * Created by iyakovlev on 13.10.17.
 */

public class LootboxNetworkClient extends NetworkClient {



    public void auth(String login, String password, ConnectionService.RequestListener<AuthResponse> listener) {
        AuthRequest authRequest = new AuthRequest(login, password);
        enqueue(authRequest, listener);
    }

    public void register(String login, String password, String name, ConnectionService.RequestListener<RegistrationResponse> listener) {
        RegistrationRequest registrationRequest = new RegistrationRequest(login, password, name);
        enqueue(registrationRequest, listener);
    }

    public void delete(FileInfo info, ConnectionService.RequestListener<FileDeleteResponse> listener) {
        FileDeleteRequest request = new FileDeleteRequest(info);
        enqueue(request, listener);
    }

    public void makeDir(String path, ConnectionService.RequestListener<MakeDirResponse> listener) {
        MakeDirRequest request = new MakeDirRequest(path);
        enqueue(request, listener);
    }

    public void getDirectory(String path, SortOrder order, ConnectionService.RequestListener<ListDirectoryResponse> listener) {
        ListDirectoryRequest listDirectoryRequest = new ListDirectoryRequest(path, order);
        enqueue(listDirectoryRequest, listener);
    }

    public void uploadFile(final Uri fileUri, final FileInfo fileInfo, final ConnectionService.RequestListener<FileUploadResponse> listener) {
        FileUploadRequest fileUploadRequest = new FileUploadRequest(fileInfo);

        enqueue(fileUploadRequest, new ConnectionService.RequestListener<FileUploadResponse>() {
            @Override
            public void onError(int errorCode) {
                listener.onError(errorCode);
            }

            @Override
            public void onResult(FileUploadResponse result) {
                Intent uploadIntent = UploaderService.getIntent(
                        getServiceContext(),
                        fileUri.toString(),
                        fileInfo.getName(),
                        result.getData().getPort(),
                        fileInfo.getSize()
                );

                getServiceContext().startService(uploadIntent);
            }
        });
    }

    public void downloadFile(final FileInfo fileInfo, final ConnectionService.RequestListener<FileDownloadResponse> listener) {
        FileDownloadRequest fileDownloadRequest = new FileDownloadRequest(fileInfo);

        enqueue(fileDownloadRequest, new ConnectionService.RequestListener<FileDownloadResponse>() {
            @Override
            public void onError(int errorCode) {
                listener.onError(errorCode);
            }

            @Override
            public void onResult(FileDownloadResponse result) {
                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + File.separator + fileInfo.getName();

                Intent downloadIntent = DownloaderService.getIntent(
                        getServiceContext(),
                        fileInfo.getName(),
                        path,
                        fileInfo.getSize(),
                        result.getData().getPort());

                getServiceContext().startService(downloadIntent);
            }
        });
    }

}
