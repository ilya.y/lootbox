package me.deft.lootbox;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import java.util.Set;

import me.deft.lootbox.network.ConnectionService;
import me.deft.lootbox.network.LootboxNetworkClient;
import me.deft.lootbox.network.NetworkClient;
import me.deft.messages.AuthResponse;

/**
 * Created by iyakovlev on 28.09.17.
 */

public class LootboxApp extends Application {

    private static final String TAG = LootboxApp.class.getSimpleName();

    private Settings mSettings;
    private LootboxNetworkClient mNetworkClient;

    @Override
    public void onCreate() {
        super.onCreate();
        mSettings = new Settings(this);
        mNetworkClient = new LootboxNetworkClient();
        mNetworkClient.connect(this);

        if (mSettings.getlLogin() != null) {
            mNetworkClient.auth(mSettings.getlLogin(), mSettings.getPassword(), new ConnectionService.RequestListener<AuthResponse>() {
                @Override
                public void onError(int errorCode) {
                    Log.d(TAG, "Error during auth");
                    mSettings.setUnauth();
                }

                @Override
                public void onResult(AuthResponse result) {
                    Log.d(TAG, "Seccessfully auth");
                }
            });
        }


    }

    public Settings getSettings() {
        return mSettings;
    }

    public LootboxNetworkClient getNetworkClient() {
        return mNetworkClient;
    }
}
