package me.deft.lootbox.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import me.deft.lootbox.R;
import me.deft.lootbox.Settings;

/**
 * Created by iyakovlev on 28.09.17.
 */

// TODO пометить активити интерфейсом и использовать во фрагментах интерфейс
public class MainActivity extends AppCompatActivity {

    private Settings mSettings;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSettings = new Settings(this);

        if (mSettings.getlLogin() == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new SigninFragment())
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, StorageFragment.getInstance(mSettings.getLastPath()))
                    .commit();
        }

    }

    public void openSignupScreen() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new SignupFragment())
                .commit();
    }

    public void openStorageScreen(String path) {
      openStorageScreen(path, true);
    }

    public void openStorageScreen(String path, boolean addToBackStack) {
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, StorageFragment.getInstance(path));
        if (addToBackStack)
            transaction.addToBackStack(path);

        transaction.commit();
    }
}

