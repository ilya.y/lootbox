package me.deft.lootbox.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import me.deft.lootbox.LootboxApp;
import me.deft.lootbox.R;
import me.deft.lootbox.Settings;
import me.deft.lootbox.network.ConnectionService;
import me.deft.lootbox.network.LootboxNetworkClient;
import me.deft.lootbox.network.NetworkClient;
import me.deft.messages.AuthRequest;
import me.deft.messages.AuthResponse;

/**
 * Created by iyakovlev on 28.09.17.
 */

public class SigninFragment extends NetworkFragment {

    private Button mSigninButton;
    private EditText mLoginEditText;
    private EditText mPasswordEditText;
    private TextView mSignupTextView;

    private Settings mSettings;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View signinView = inflater.inflate(R.layout.fragment_signin, container, false);

        mSettings = ((LootboxApp)getActivity().getApplication()).getSettings();


        mLoginEditText = signinView.findViewById(R.id.signin_fragment_login_field);
        mPasswordEditText = signinView.findViewById(R.id.signin_fragment_password_field);
        mSigninButton = signinView.findViewById(R.id.signin_fragment_signin_button);
        mSignupTextView = signinView.findViewById(R.id.signin_fragment_signup_link);
        initViews();
        return signinView;
    }


    private void initViews() {
        mSigninButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLoginEditText.setError(null);
                mPasswordEditText.setError(null);
                final String login = mLoginEditText.getText().toString();
                final String password = mPasswordEditText.getText().toString();

                mNetworkClient.auth(login, password, new ConnectionService.RequestListener<AuthResponse>() {
                    @Override
                    public void onError(int errorCode) {
                        switch (errorCode) {
                            case AuthResponse.ERROR_USER_NOT_FOUND:
                                mLoginEditText.requestFocus();
                                mLoginEditText.setError(getString(R.string.login_error_user_not_found));
                                break;

                            case AuthResponse.ERROR_WRONG_PASSWORD:
                                mPasswordEditText.requestFocus();
                                mPasswordEditText.setError(getString(R.string.login_error_wrong_password));
                                break;
                            default:
                                mLoginEditText.setError("Unknown error");
                        }

                    }

                    @Override
                    public void onResult(AuthResponse result) {
                        mSettings.saveLogin(login);
                        mSettings.savePassword(password);
                        ((MainActivity)getActivity()).openStorageScreen(mSettings.getLastPath(), false);
                    }
                });
            }
        });

        mSignupTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).openSignupScreen();
            }
        });
    }
}
