package me.deft.lootbox.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import me.deft.data.FileInfo;
import me.deft.lootbox.FileTransmissionBroadcastReceiver;
import me.deft.lootbox.LootboxApp;
import me.deft.lootbox.R;
import me.deft.lootbox.Settings;
import me.deft.lootbox.network.ConnectionService;
import me.deft.lootbox.network.LootboxNetworkClient;
import me.deft.lootbox.network.NetworkClient;
import me.deft.lootbox.utils.FileUtils;
import me.deft.lootbox.utils.PathUtils;
import me.deft.messages.FileDeleteResponse;
import me.deft.messages.FileDownloadResponse;
import me.deft.messages.FileUploadResponse;
import me.deft.messages.ListDirectoryResponse;
import me.deft.messages.MakeDirResponse;
import me.deft.misc.SortOrder;

/**
 * Created by iyakovlev on 28.09.17.
 */

public class StorageFragment extends NetworkFragment
        implements  ConnectionService.RequestListener<ListDirectoryResponse> {

    private static final int REQUEST_CODE_OPEN_FILE = 2001;
    private static final String EXTRA_PATH = "extra_path";
    private static final String TAG = StorageFragment.class.getSimpleName();

    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private FloatingActionButton mFab;

    private StorageAdapter mStorageAdapter;
    private Settings mSettings;
    private String mPath;

    private LootboxNetworkClient mNetworkClient;
    private FileTransmissionBroadcastReceiver mTransmissionBroadcastReceiver;

    private NetworkClient.ConnectionStatusListener mConnectionStatusListener = new NetworkClient.ConnectionStatusListener() {
        @Override
        public void onConnected() {
            loadCurrentDirectory();
        }

        @Override
        public void onConnecting() {

        }

        @Override
        public void onDisconnected() {

        }
    };


    public static StorageFragment getInstance(String path) {

        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_PATH, path);

        StorageFragment fragment = new StorageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_storage, container, false);

        mToolbar = rootView.findViewById(R.id.storage_toolbar);
        mRecyclerView = rootView.findViewById(R.id.storage_recyclerview);
        mFab = rootView.findViewById(R.id.storage_fab_file);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mSettings = ((LootboxApp)getActivity().getApplication()).getSettings();

        mPath = getArguments().getString(EXTRA_PATH);

        if (mPath == null)
            throw new IllegalStateException("Fragment should be called with path provided");

        mStorageAdapter = new StorageAdapter();
        mRecyclerView.setAdapter(mStorageAdapter);

        mNetworkClient = ((LootboxApp)getActivity().getApplication()).getNetworkClient();
        mNetworkClient.addClientStatusListener(mConnectionStatusListener, true);

        mTransmissionBroadcastReceiver = new FileTransmissionBroadcastReceiver(new FileTransmissionBroadcastReceiver.TransmissionListener() {
            @Override
            public void onFileUploaded(String path) {
                //if (path.equals(mPath)) // надо в сервис пробрасывать папку, в котору мы загружаем файл
                    loadCurrentDirectory();
            }
        });


        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
               mTransmissionBroadcastReceiver, FileTransmissionBroadcastReceiver.getIntentFilter()
        );

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openFileIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                openFileIntent.addCategory(Intent.CATEGORY_OPENABLE);
                openFileIntent.setType("*/*");
                startActivityForResult(openFileIntent, REQUEST_CODE_OPEN_FILE);
            }
        });

        mStorageAdapter.setFileActionListener(new StorageAdapter.FileActionListener() {
            @Override
            public void onFileClick(final FileInfo fileInfo, int position) {
                switch (fileInfo.getType()) {
                    case FILE: downloadFile(fileInfo); break;
                    case DIRECTORY: openDirectory(fileInfo); break;
                }
            }

            @Override
            public void onFileLongClick(FileInfo fileInfo, int position) {
                openContextMenu(fileInfo);
            }
        });
        Toolbar toolbar = rootView.findViewById(R.id.storage_toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        return rootView;
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_OPEN_FILE) {
            if (data != null) {
                final Uri uri = data.getData();
                FileUtils utils = new FileUtils(getContext());
                final FileInfo fileInfo = utils.getFileInfo(uri);
                fileInfo.setPath(mPath);

                mNetworkClient.uploadFile(uri, fileInfo, new ConnectionService.RequestListener<FileUploadResponse>() {
                    @Override
                    public void onError(int errorCode) {
                        throw new IllegalStateException("Show error");
                    }

                    @Override
                    public void onResult(FileUploadResponse result) {
                        throw new IllegalStateException("Show success");
                    }
                });
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mNetworkClient.removeClientStatusListener(mConnectionStatusListener);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.storage_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_new_folder) {
            showMakeDirDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openContextMenu(final FileInfo info) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final String[] items = getResources().getStringArray(R.array.file_context_menu_items);
        builder.setTitle(getString(R.string.file_context_dialog_title) + info.getName());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            // TODO это какая-то хрень
            @Override
            public void onClick(DialogInterface dialogInterface, final int i) {
                final String selectedItem = items[i];
                if (selectedItem.equals(getResources().getString(R.string.file_context_menu_info))){
                    return;
                }

                if (selectedItem.equals(getResources().getString(R.string.file_context_menu_delete))){
                    mNetworkClient.delete(info, new ConnectionService.RequestListener<FileDeleteResponse>() {
                        @Override
                        public void onError(int errorCode) {
                            Snackbar.make(getView(), getString(R.string.storage_fragment_error_deleting_file) + info.getName(), Snackbar.LENGTH_SHORT)
                                    .show();
                        }

                        @Override
                        public void onResult(FileDeleteResponse result) {
                            if (result.getData()) {
                                Snackbar.make(getView(), info.getName() + getString(R.string.storage_fragment_file_deleted), Snackbar.LENGTH_SHORT)
                                        .show();
                                loadCurrentDirectory();
                            }
                        }
                    });
                    return;
                }
            }
        }).create().show();
    }

    @Override
    public void onError(int errorCode) {
        throw new IllegalStateException("hm");
    }

    @Override
    public void onResult(ListDirectoryResponse result) {
       mStorageAdapter.setFiles(result.getData().getFiles());
       mStorageAdapter.notifyDataSetChanged();
    }


    private void loadCurrentDirectory() {
       loadDirectory(mPath, SortOrder.NAME_ASC);
    }

    private void loadDirectory(String path, SortOrder order) {
        mNetworkClient.getDirectory(path, order, new ConnectionService.RequestListener<ListDirectoryResponse>() {
            @Override
            public void onError(int errorCode) {
                Log.d(TAG, "onError: ");
            }

            @Override
            public void onResult(ListDirectoryResponse result) {
                mStorageAdapter.setFiles(result.getData().getFiles());
                mStorageAdapter.notifyDataSetChanged();
            }
        });
    }

    private void downloadFile(FileInfo fileInfo) {
        mNetworkClient.downloadFile(fileInfo, new ConnectionService.RequestListener<FileDownloadResponse>() {
            @Override
            public void onError(int errorCode) {
                throw new IllegalStateException("Show error");
            }

            @Override
            public void onResult(FileDownloadResponse result) {
                throw new IllegalStateException("Show success");
            }
        });
    }

    private void openDirectory(FileInfo info) {
        ((MainActivity)getActivity()).openStorageScreen(PathUtils.down(mPath, info.getName()));
    }

    private void showMakeDirDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final EditText editText = new EditText(getContext());
        builder.setTitle(R.string.storage_fragment_makedir_title)
                .setView(editText)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final String dirName = editText.getText().toString();

                        if (dirName == null || dirName.isEmpty())
                            return;

                        final String path = PathUtils.down(mPath, dirName);
                        mNetworkClient.makeDir(path, new ConnectionService.RequestListener<MakeDirResponse>() {
                            @Override
                            public void onError(int errorCode) {
                                throw new IllegalStateException("todo");
                            }

                            @Override
                            public void onResult(MakeDirResponse result) {
                                loadCurrentDirectory();
                            }
                        });
                    }
                }).show();
    }
}
