package me.deft.lootbox.ui;

import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

import me.deft.data.FileInfo;
import me.deft.data.FileType;
import me.deft.lootbox.R;

/**
 * Created by iyakovlev on 29.09.17.
 */

public class StorageAdapter extends RecyclerView.Adapter<StorageAdapter.FileItemViewHolder> {

    public interface FileActionListener{
        void onFileClick(FileInfo fileInfo, int position);
        void onFileLongClick(FileInfo fileInfo, int position);
    }

    private List<FileInfo> mFiles;
    private FileActionListener mFileActionListener;

    public void setFileActionListener(FileActionListener fileActionListener) {
        mFileActionListener = fileActionListener;
    }

    public void removeFileActionListener() {
        mFileActionListener = null;
    }

    @Override
    public FileItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_file, parent, false);

        return new FileItemViewHolder(view);
    }

    public void setFiles(List<FileInfo> files) {
        mFiles = files;
    }

    @Override
    public void onBindViewHolder(FileItemViewHolder holder, final int position) {
        holder.setUp(mFiles.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFileActionListener != null)
                    mFileActionListener.onFileClick(mFiles.get(position), position);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mFileActionListener != null)
                    mFileActionListener.onFileLongClick(mFiles.get(position), position);
                return true;
            }
        });
    }

    @Override
    public void onViewRecycled(FileItemViewHolder holder) {
        super.onViewRecycled(holder);
        holder.mTextView.setOnClickListener(null);
        holder.mTextView.setOnLongClickListener(null);
    }

    @Override
    public int getItemCount() {
        return mFiles == null ? 0 : mFiles.size();
    }

    public static class FileItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImageView;
        private TextView mTextView;

        public FileItemViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.item_icon);
            mTextView = itemView.findViewById(R.id.item_text);
        }

        public void setUp(FileInfo info) {
            switch (info.getType()) {
                case FILE:  mImageView.setImageResource(R.drawable.ic_file); break;
                case DIRECTORY:  mImageView.setImageResource(R.drawable.ic_folder); break;
                default:  mImageView.setImageResource(R.drawable.ic_file);
            }

            mTextView.setText(info.getName());
        }
    }


}
