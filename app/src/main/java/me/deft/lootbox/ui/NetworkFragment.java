package me.deft.lootbox.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;

import me.deft.lootbox.LootboxApp;
import me.deft.lootbox.network.LootboxNetworkClient;
import me.deft.lootbox.network.NetworkClient;

/**
 * Created by iyakovlev on 15.10.2017.
 */

public class NetworkFragment extends Fragment implements NetworkClient.ConnectionStatusListener {

    protected LootboxNetworkClient mNetworkClient;
    private boolean mIsConnected;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!(getActivity().getApplication() instanceof LootboxApp))
            throw new IllegalStateException("Fragment should be instantiated in within LootboxApp");

         mNetworkClient = ((LootboxApp)getActivity().getApplication()).getNetworkClient();
         mIsConnected = mNetworkClient.isConnected();
         mNetworkClient.addClientStatusListener(this, true);
    }



    @Override
    public void onConnected() {
        if (getView() != null && !mIsConnected)
            Snackbar.make(getView(), "Connected", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onDisconnected() {
        if (getView() != null)
            Snackbar.make(getView(), "Not connected", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Reconnect", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mNetworkClient.connect(getContext());
                        }
                    })
                    .show();
    }

    @Override
    public void onConnecting() {
        if (getView() != null)
            Snackbar.make(getView(), "Connecting...", Snackbar.LENGTH_INDEFINITE).show();
    }



}
