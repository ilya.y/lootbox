package me.deft.lootbox.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import me.deft.lootbox.LootboxApp;
import me.deft.lootbox.R;
import me.deft.lootbox.Settings;
import me.deft.lootbox.network.ConnectionService;
import me.deft.lootbox.network.NetworkClient;
import me.deft.messages.RegistrationResponse;

/**
 * Created by iyakovlev on 28.09.17.
 */

public class SignupFragment extends NetworkFragment implements ConnectionService.RequestListener<RegistrationResponse> {

    private Button mSignupButton;
    private EditText mLoginEditText;
    private EditText mPasswordEditText;
    private EditText mNameEditText;

    private Settings mSettings;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);

        mSignupButton = view.findViewById(R.id.signup_fragment_signin_button);
        mLoginEditText = view.findViewById(R.id.signup_fragment_login_field);
        mPasswordEditText = view.findViewById(R.id.signup_fragment_password_field);
        mNameEditText = view.findViewById(R.id.signup_fragment_name_field);

        mSettings = ((LootboxApp)getActivity().getApplication()).getSettings();

        mSignupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLoginEditText.setError(null);
                signup();
            }
        });

        return view;
    }

    private void signup() {
        final String login = mLoginEditText.getText().toString();
        final String password = mPasswordEditText.getText().toString();
        final String name = mNameEditText.getText().toString();

        mNetworkClient.register(login, password, name, this);
    }

    @Override
    public void onError(int errorCode) {
        switch (errorCode) {
            case RegistrationResponse.ERROR_USER_EXIST:
                mLoginEditText.requestFocus();
                mLoginEditText.setError(getString(R.string.signup_fragment_error_user_exist));
                break;
            case RegistrationResponse.ERROR_CANT_CREATE_USER:
                mLoginEditText.requestFocus();
                mLoginEditText.setError(getString(R.string.signup_fragment_error_cant_create));
                break;
            default:
                mLoginEditText.setError(getString(R.string.signup_fragment_error_unknown_error));
        }
    }

    @Override
    public void onResult(RegistrationResponse result) {
        mSettings.saveLogin(result.getData().getLogin());
        mSettings.savePassword(result.getData().getPassword());

        ((MainActivity)getActivity()).openStorageScreen(mSettings.getLastPath(), false);
    }
}


