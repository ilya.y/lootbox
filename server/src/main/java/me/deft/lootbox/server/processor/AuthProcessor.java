package me.deft.lootbox.server.processor;

import me.deft.data.User;
import me.deft.lootbox.server.repo.UserRepository;
import me.deft.lootbox.server.utils.FileUtils;
import me.deft.messages.AuthRequest;
import me.deft.messages.AuthResponse;

/**
 * Created by iyakovlev on 19.09.17.
 */

public class AuthProcessor extends AbstractProcessor<AuthRequest, AuthResponse> {

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void process(RequestWrapper<AuthRequest> request, ProcessorListener<AuthResponse> listener) {
        final String login = request.getRequest().getLogin();
        final String password = request.getRequest().getPassword();
        User res = UserRepository.getInstance().getUser(login, password);
        new FileUtils(res).createHomeDir();
        AuthResponse response = new AuthResponse(request.getRequest());

        if (res != null) {
            request.getClientSession().setUser(res);
            response.setData(res);
            onSuccess(response, listener);
        } else {
            if (UserRepository.getInstance().checkUserExist(login)) {
                onError(response, AuthResponse.ERROR_WRONG_PASSWORD, listener);
            } else {
                onError(response, AuthResponse.ERROR_USER_NOT_FOUND, listener);
            }

        }
    }


}
