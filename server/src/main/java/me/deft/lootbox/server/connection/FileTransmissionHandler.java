package me.deft.lootbox.server.connection;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.concurrent.Executor;

import me.deft.data.ConnectionInfo;
import me.deft.data.FileInfo;
import me.deft.lootbox.server.ClientSession;
import me.deft.lootbox.server.LootboxServer;
import me.deft.lootbox.server.utils.FileUtils;
import me.deft.lootbox.server.utils.Log;

/**
 * Created by iyakovlev on 21.09.17.
 */

public class FileTransmissionHandler {

    public static final int TIMEOUT_30_SECONDS = 30_000; // ждем коннекта 30 секунд
    private final Executor mExecutor = LootboxServer.getInstance().execute().io();
    private FileInfo mFileInfo;
    private ClientSession mClientSession;
    private ConnectionInfo mConnectionInfo;
    private FileUtils mFileUtils;

    public FileTransmissionHandler(FileInfo fileInfo, ClientSession clientSession, ConnectionInfo connectionInfo) {
        mFileInfo = fileInfo;
        mClientSession = clientSession;
        mConnectionInfo = connectionInfo;
        mFileUtils = new FileUtils(mClientSession.getUser());
    }

    public interface ResultListener {
        void onFinished(boolean result);
    }

    public void receive(final int timeout, final ResultListener listener) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                boolean result = true;
                try(ServerSocket serverSocket = new ServerSocket(mConnectionInfo.getPort());
                        InputStream is = serverSocket.accept().getInputStream();
                        FileOutputStream os = mFileUtils.getFileOutputStream(mFileInfo)) {

                    serverSocket.setSoTimeout(timeout);
                    if (os != null) {
                        int read;
                        int totalRead = 0;
                        byte[] buffer = new byte[4096];
                        while ((read = is.read(buffer)) != -1) {
                            os.write(buffer, 0, read);
                            os.flush();
                            totalRead += read;
                            Log.log("saved: " + totalRead);
                        }

                    }
                } catch (IOException e) {
                    result = false;
                    mFileUtils.removeFile(mFileInfo);
                } finally {
                    listener.onFinished(result);
                }

            }
        });
    }

    public void send(final int timeout, final ResultListener listener) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                boolean result = true;

                try (ServerSocket serverSocket = new ServerSocket(mConnectionInfo.getPort());
                     OutputStream outputStream = new BufferedOutputStream(serverSocket.accept().getOutputStream());
                     FileInputStream inputStream = mFileUtils.getFileInputStream(mFileInfo)){
                    serverSocket.setSoTimeout(timeout);

                    Log.log("Socket created. Waiting for connection");

                    Log.log("Open fileInputstraem");

                    if (inputStream != null) {
                        int read;
                        byte[] buffer = new byte[8192];
                        while ((read = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, read);
                            outputStream.flush();
                        }


                    } else {
                        Log.log("Unable to upload file " + mFileInfo.getName());
                    }
                } catch (IOException e) {
                    result = false;
                    Log.log("Unable to upload file " + mFileInfo.getName());
                } finally {
                    listener.onFinished(result);
                }

            }
        });
    }
}
