package me.deft.lootbox.server.processor;

import me.deft.lootbox.server.utils.FileUtils;
import me.deft.messages.AbstractResponse;
import me.deft.messages.MakeDirRequest;
import me.deft.messages.MakeDirResponse;

/**
 * Created by iyakovlev on 17.10.17.
 */

public class MakeDirProcessor extends AbstractProcessor<MakeDirRequest, MakeDirResponse> {

    @Override
    public void process(RequestWrapper<MakeDirRequest> request, ProcessorListener<MakeDirResponse> listener) {
        final String path = request.getRequest().getDirPath();
        FileUtils utils = new FileUtils(request.getClientSession().getUser());
        boolean res = utils.createDir(path);
        MakeDirResponse response = new MakeDirResponse(request.getRequest());
        if (res) {
            onSuccess(response, listener);
        } else {
            onError(response, AbstractResponse.ERROR_IO, listener);
        }
    }

    @Override
    public boolean needAuth() {
        return true;
    }
}
