package me.deft.lootbox.server;

import java.io.IOException;

public class Main {

    static LootboxServer mLootboxServer;

    public static void main(String...args) {

        try {
            mLootboxServer = LootboxServer.getInstance();
            mLootboxServer.startListeningForConnections();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
