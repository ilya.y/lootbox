package me.deft.lootbox.server.repo;

import me.deft.data.User;
import me.deft.lootbox.server.utils.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRepository {

    private static final String DRIVER = "org.sqlite.JDBC";
    private static final String DB = "jdbc:sqlite:users.db";
    private static UserRepository sInstance;
    private static final String TABLE_NAME = "Users";

    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (\n" +
                    "    id       INTEGER PRIMARY KEY\n" +
                    "                     NOT NULL,\n" +
                    "    login    TEXT    UNIQUE,\n" +
                    "    password TEXT,\n" +
                    "    name     TEXT\n" +
                    ");";

    private static final String SQL_GET_USER =
            "SELECT login, password, name FROM " + TABLE_NAME + " WHERE " +
                    " login = ? AND password = ?;";

    private static final String SQL_GET_USER_COUNT =
            "SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE " +
                    " login = ?;";

    private static final String SQL_ADD_USER = "INSERT INTO " + TABLE_NAME + " (\n" +
            "login, password, name) VALUES (?, ?, ?)";

    // т.к. репозиторий у нас синглтон, а дергаться он может из разных потоков
    // то оборачиваем соединение в ThreadLocal, что бы в разных потоках соединение было
    // гарантировано разным
    private ThreadLocal<Connection> mDbConnection;
    private ThreadLocal<PreparedStatement> mStatement;

    public static UserRepository getInstance() {
        if (sInstance == null)
            sInstance = new UserRepository();
        return sInstance;
    }

    public UserRepository() {
        mDbConnection = new ThreadLocal<>();
        mStatement = new ThreadLocal<>();

        init();
    }

    private void init() {
        try {
            Class.forName(DRIVER);
            connect();
            setQuery(SQL_CREATE_TABLE);
            mStatement.get().execute();
            disconnect();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    //TODO сделать ConnectionPool
    private boolean connect() throws SQLException {
        mDbConnection.set(DriverManager.getConnection(DB));

        return true;
    }

    private boolean disconnect() {
        try {
            mStatement.get().close();
            mStatement.remove();
            mDbConnection.get().close();
            mDbConnection.remove();
            return true;
        } catch (SQLException e) {
            Log.log(e.getErrorCode() +" : " + e.getMessage());
        }
        return false;
    }

    private boolean setQuery(String sql) {
        if (mDbConnection.get() != null) {
            try {
                mStatement.set(mDbConnection.get().prepareStatement(sql));
                return true;
            } catch (SQLException e) {
                Log.log(e.getMessage());
                return false;
            }
        }

        return false;
    }

    public boolean addUser(User user) {

        boolean res = true;
        try {
            if (connect() && setQuery(SQL_ADD_USER)) {
                mStatement.get().setString(1, user.getLogin());
                mStatement.get().setString(2, user.getPassword());
                mStatement.get().setString(3, user.getName());
                mStatement.get().execute();
            }

        } catch (SQLException e) {
            Log.log("Add user: " + e.getMessage());
            res = false;
        } finally {
            disconnect();
        }
        return res;
    }

    public User getUser(String login, String password) {
        try {
            if (checkUserExist(login)) {
                connect();
                setQuery(SQL_GET_USER);
                mStatement.get().setString(1, login);
                mStatement.get().setString(2, password);
                ResultSet res = mStatement.get().executeQuery();
                return new User(
                        res.getString(1),
                        res.getString(2),
                        res.getString(3)
                );
            } else {
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean checkUserExist(String login) {
        boolean res = false;
        try {
            connect();
            setQuery(SQL_GET_USER_COUNT);
            mStatement.get().setString(1, login);
            res = mStatement.get().executeQuery().getInt(1) == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            disconnect();
        }

        return res;
    }
}
