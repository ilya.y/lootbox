package me.deft.lootbox.server.processor;

import me.deft.data.DirectoryInfo;
import me.deft.lootbox.server.utils.FileUtils;
import me.deft.messages.ListDirectoryRequest;
import me.deft.messages.ListDirectoryResponse;

/**
 * Created by iyakovlev on 20.09.17.
 */

public class ListDirectoryProcessor extends AbstractProcessor<ListDirectoryRequest, ListDirectoryResponse> {

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void process(RequestWrapper<ListDirectoryRequest> request, ProcessorListener<ListDirectoryResponse> listener) {
        FileUtils fileUtils = new FileUtils(request.getClientSession().getUser());
        DirectoryInfo dir = fileUtils.getDirectory(request.getRequest().getPath());
        ListDirectoryResponse response = new ListDirectoryResponse(request.getRequest());



        response.setData(dir);

        onSuccess(response, listener);
    }
}
