package me.deft.lootbox.server.connection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.concurrent.Executor;

import me.deft.lootbox.server.ClientSession;
import me.deft.lootbox.server.LootboxServer;
import me.deft.lootbox.server.processor.Processor;
import me.deft.lootbox.server.processor.RequestWrapper;
import me.deft.lootbox.server.utils.Log;
import me.deft.messages.Request;
import me.deft.messages.Response;

public class ClientConnectionHandler {

    private static final RequestDispatcher sRequestDispatcher = new RequestDispatcher();

    private ObjectInputStream mInputStream;
    private ClientSession mClientSession;
    private ObjectOutputStream mOutputStream;
    private Executor mExecutor;

    private boolean mStarted;

    public ClientConnectionHandler(ClientSession session, Executor executor) throws IOException{
        mExecutor = executor;
        mClientSession = session;
        mStarted = false;
    }

    public void startListening() {
        mExecutor.execute(this::go);
    }

    private void go() {

        if (mStarted)
            throw new IllegalStateException("Handler already started");

        mStarted = true;
        try {
            mOutputStream = new ObjectOutputStream(mClientSession.getConnectionSocket().getOutputStream());
            mOutputStream.flush();
            mInputStream = new ObjectInputStream(mClientSession.getConnectionSocket().getInputStream());
            while (mStarted) {
                try {
                    final Request request = (Request)mInputStream.readObject();
                    Log.log("Connection Handler: request: " + request.getClass().getSimpleName());
                    sRequestDispatcher.process(new RequestWrapper(request, mClientSession), new Processor.ProcessorListener() {
                        @Override
                        public void onProcessed(Object result) {
                            sendResponse((Response)result);
                        }
                    });
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            //  e.printStackTrace();
            Log.log("Stopping client session");
            stopListening();
        }
    }

    public void stopListening() {
        mStarted = false;
        LootboxServer.getInstance().getSessionStorage().removeConnection(this);
    }

    private void sendResponse(Response response) {
        try {
            mOutputStream.writeObject(response);
            mOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ClientSession getClientSession() {
        return mClientSession;
    }
}
