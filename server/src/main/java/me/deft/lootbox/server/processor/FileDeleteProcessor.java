package me.deft.lootbox.server.processor;

import me.deft.lootbox.server.utils.FileUtils;
import me.deft.messages.FileDeleteRequest;
import me.deft.messages.FileDeleteResponse;

/**
 * Created by iyakovlev on 15.10.2017.
 */

public class FileDeleteProcessor extends AbstractProcessor<FileDeleteRequest, FileDeleteResponse> {

    @Override
    public void process(RequestWrapper<FileDeleteRequest> request, ProcessorListener<FileDeleteResponse> listener) {
            FileUtils utils = new FileUtils(request.getClientSession().getUser());
        boolean result = utils.removeFile(request.getRequest().getFileInfo());
        FileDeleteResponse response = new FileDeleteResponse(request.getRequest());
        response.setData(result);
        listener.onProcessed(response);
    }

    @Override
    public boolean needAuth() {
        return true;
    }
}
