package me.deft.lootbox.server.processor;

import me.deft.data.User;
import me.deft.lootbox.server.utils.FileUtils;
import me.deft.lootbox.server.repo.UserRepository;
import me.deft.lootbox.server.utils.Log;
import me.deft.messages.RegistrationRequest;
import me.deft.messages.RegistrationResponse;

/**
 * Created by iyakovlev on 13.09.17.
 */

public class RegistrationProcessor extends AbstractProcessor<RegistrationRequest, RegistrationResponse> {

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void process(RequestWrapper<RegistrationRequest> request, ProcessorListener<RegistrationResponse> listener) {
        RegistrationResponse r = new RegistrationResponse(request.getRequest());
        if (UserRepository.getInstance().checkUserExist(request.getRequest().getLogin())) {
            onError(r, RegistrationResponse.ERROR_USER_EXIST, listener);
            Log.log("User exists");
            return;
        }

        User user = new User(
                request.getRequest().getLogin(),
                request.getRequest().getPassword(),
                request.getRequest().getName()
        );
        if (UserRepository.getInstance().addUser(user)) {
            if (new FileUtils(user).createHomeDir()) {
                request.getClientSession().setUser(user);
                r.setData(user);
                onSuccess(r, listener);
                Log.log("User added");
            } else {
                Log.log("Can't create home dir");
            }


        } else {
            onError(r, RegistrationResponse.ERROR_CANT_CREATE_USER, listener);
            Log.log("Error save user");
        }
    }
}
