package me.deft.lootbox.server;

import me.deft.data.User;

import java.net.Socket;

public interface ClientSession {

    interface SessionClosedListener {
        void onSessionClosed();
    }

    boolean isAuthorized();
    void close();
    User getUser();
    void setUser(User user);
    Socket getConnectionSocket();
    void addSessionClosedListener(SessionClosedListener listener);
    void destroy();
}
