package me.deft.lootbox.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;

import me.deft.ServerConfig;
import me.deft.data.ConnectionInfo;
import me.deft.lootbox.server.connection.ConnectionPool;
import me.deft.lootbox.server.utils.FileUtils;
import me.deft.lootbox.server.repo.UserRepository;

public class LootboxServer {

    private static LootboxServer sInstance;

    ServerExecutors mExecutors;
    ServerSocket mServerConnectionSocket;
    ClientSessionFactory mClientSessionFactory;
    SessionStorage2 mSessionStorage;
    ConnectionPool mConnectionPool = new ConnectionPool();

    boolean mListening;

    public static synchronized LootboxServer getInstance() {
        if (sInstance == null) {
            sInstance = new LootboxServer();
        }
        return sInstance;
    }

    public ConnectionPool getConnectionPool() {
        return mConnectionPool;
    }

    private LootboxServer() {
        FileUtils.createMainStorage();
        try {
            mServerConnectionSocket = new ServerSocket(
                    ServerConfig.PORT,
                    0,
                    InetAddress.getByName(ServerConfig.ADDRESS_LOCAL)
                    );
        } catch (IOException e) {
            throw new IllegalStateException("Unable to start server: " + e.getMessage());
        }
        mClientSessionFactory = new ClientSessionFactory(
                Executors.newCachedThreadPool()
        );

        mExecutors = new ServerExecutors(
                Executors.newCachedThreadPool(),
                Executors.newCachedThreadPool()
        );

        mSessionStorage = new SessionStorage2(mClientSessionFactory);
    }

    public void startListeningForConnections() throws IOException{
        mListening = true;

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (mListening) {
                    try {
                        Socket clientSocket = mServerConnectionSocket.accept();
                        mSessionStorage.addNewConnection(clientSocket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void stopListeningForConnections() throws IOException {
        mListening = false;
        mServerConnectionSocket.close();
    }

    public ServerExecutors execute() {
        return mExecutors;
    }


    public SessionStorage2 getSessionStorage() {
        return mSessionStorage;
    }
}
