package me.deft.lootbox.server.processor;

import me.deft.data.ConnectionInfo;
import me.deft.data.FileInfo;
import me.deft.lootbox.server.LootboxServer;
import me.deft.lootbox.server.connection.FileTransmissionHandler;
import me.deft.messages.FileUploadRequest;
import me.deft.messages.FileUploadResponse;

import static me.deft.messages.FileUploadResponse.ERROR_UPLOAD_FAILED;

/**
 * Created by iyakovlev on 29.09.17.
 */

public class FileUploadProcessor extends AbstractProcessor<FileUploadRequest, FileUploadResponse> {

    @Override
    public void process(RequestWrapper<FileUploadRequest> request, ProcessorListener<FileUploadResponse> listener) {
        FileInfo fileInfo = request.getRequest().getFileInfo();
        FileUploadResponse response = new FileUploadResponse(request.getRequest());
        final ConnectionInfo info;
        try {
            info = LootboxServer.getInstance().getConnectionPool().get();
            response.setData(info);
            onSuccess(response, listener);
        } catch (InterruptedException e) {
            onError(response, ERROR_UPLOAD_FAILED, listener);
            return;
        }

        new FileTransmissionHandler(
                fileInfo,
                request.getClientSession(),
                info
        ).receive(FileTransmissionHandler.TIMEOUT_30_SECONDS, new FileTransmissionHandler.ResultListener() {
            @Override
            public void onFinished(boolean result) {
                LootboxServer.getInstance().getConnectionPool().recycle(info);
            }
        });
    }

    @Override
    public boolean needAuth() {
        return true;
    }
}
