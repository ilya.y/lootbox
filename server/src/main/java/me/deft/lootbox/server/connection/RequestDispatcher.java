package me.deft.lootbox.server.connection;

import java.util.HashMap;
import java.util.Map;

import me.deft.lootbox.server.processor.AuthProcessor;
import me.deft.lootbox.server.processor.FileDeleteProcessor;
import me.deft.lootbox.server.processor.FileDownloadProcessor;
import me.deft.lootbox.server.processor.FileUploadProcessor;
import me.deft.lootbox.server.processor.ListDirectoryProcessor;
import me.deft.lootbox.server.processor.MakeDirProcessor;
import me.deft.lootbox.server.processor.Processor;
import me.deft.lootbox.server.processor.RegistrationProcessor;
import me.deft.lootbox.server.processor.RequestWrapper;
import me.deft.lootbox.server.utils.Log;
import me.deft.messages.AuthRequest;
import me.deft.messages.FileDeleteRequest;
import me.deft.messages.FileUploadRequest;
import me.deft.messages.FileDownloadRequest;
import me.deft.messages.ListDirectoryRequest;
import me.deft.messages.MakeDirRequest;
import me.deft.messages.RegistrationRequest;
import me.deft.messages.Request;

/**
 * Created by iyakovlev on 13.09.17.
 */

public class RequestDispatcher {

    private static final Map<Class<? extends Request>,Processor> sProcessorMapMap = new HashMap<>();

    static {
        sProcessorMapMap.put(RegistrationRequest.class, new RegistrationProcessor());
        sProcessorMapMap.put(AuthRequest.class, new AuthProcessor());
        sProcessorMapMap.put(ListDirectoryRequest.class, new ListDirectoryProcessor());
        sProcessorMapMap.put(FileDownloadRequest.class, new FileDownloadProcessor());
        sProcessorMapMap.put(FileUploadRequest.class, new FileUploadProcessor());
        sProcessorMapMap.put(FileDeleteRequest.class, new FileDeleteProcessor());
        sProcessorMapMap.put(MakeDirRequest.class, new MakeDirProcessor());
    }

    // TODO переписать это на аннотации
    public void process(final RequestWrapper request, final Processor.ProcessorListener listener) {
        final Processor processor =  sProcessorMapMap.get(request.getRequest().getClass());
        if (processor == null) {
            Log.log("Unknown request");
            return;
        }
        if (processor.needAuth() && !request.getClientSession().isAuthorized()) {
            Log.log("Client has to be authorized to perform " + request.getRequest().getClass().getSimpleName() + " request");
            //TODO сделать обработку ошибок типа авторизации
            return;
        }

        Log.log("Incoming request: "  + request.getRequest().getClass().getSimpleName());
        if (processor != null) {
            processor.process(request, listener);
        } else {
            Log.log("Unknown request: " + request.getRequest().getClass().toString());
        }
    }


}
