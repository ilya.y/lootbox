package me.deft.lootbox.server;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.Executor;

import me.deft.data.User;


public class ClientSessionFactory {

    // общий для всех клиентов экзекутор
    private Executor mSharedExecutor;

    public ClientSessionFactory(Executor sharedExecutor) {
        mSharedExecutor = sharedExecutor;
    }

    public ClientSession getClient(Socket socket) {
        try {
            return new ClientSessionImpl(socket);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *  Класс для хранения соединения с клиентом
     *
     */
    private static class ClientSessionImpl implements ClientSession {

        private final Socket mConnectionSocket;

        private SessionClosedListener mSessionClosedListener;
        private User mUser;

        public ClientSessionImpl(Socket socket) throws IOException{
            mConnectionSocket = socket;
        }

        @Override
        public User getUser() {
            return mUser;
        }

        @Override
        public void setUser(User user) {
            mUser = user;
        }

        @Override
        public boolean isAuthorized() {
            return  mUser != null;
        }

        @Override
        public void close() {
            try {
                mConnectionSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        @Override
        public Socket getConnectionSocket() {
            return mConnectionSocket;
        }

        @Override
        public void addSessionClosedListener(SessionClosedListener listener) {
            mSessionClosedListener = listener;
        }


        @Override
        public void destroy() {
            try {
                mConnectionSocket.close();
            } catch (IOException e) {
                throw new Error("Error closing session");
            }
        }
    }

}
