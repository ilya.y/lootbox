package me.deft.lootbox.server.processor;

import me.deft.messages.Request;
import me.deft.messages.Response;

/**
 * Created by iyakovlev on 18.09.17.
 */

public abstract class AbstractProcessor <T extends Request<R>, R extends Response>
        implements Processor<T, R> {

    void onSuccess(R response, ProcessorListener<R> listener) {
        listener.onProcessed(response);
    }

    void onError(R response, int errorCode, ProcessorListener<R> listener) {
        response.setError(errorCode);
        listener.onProcessed(response);
    }
}
