package me.deft.lootbox.server;

import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import me.deft.lootbox.server.connection.ClientConnectionHandler;

/**
 * Created by iyakovlev on 18.09.17.
 */

public class SessionStorage2 {

    List<ClientConnectionHandler> mHandlers;
    private ClientSessionFactory mClientSessionFactory;
    private final Executor mClientsExecutor = Executors.newCachedThreadPool();

    public SessionStorage2(ClientSessionFactory clientSessionFactory) {
        mClientSessionFactory = clientSessionFactory;
        mHandlers = new LinkedList<>();

    }

    public void removeConnection(ClientConnectionHandler handler) {
        mHandlers.remove(handler);
    }

    public void addNewConnection(Socket socket) {
        try {
            ClientConnectionHandler handler = new ClientConnectionHandler(
                    mClientSessionFactory.getClient(socket),
                    mClientsExecutor);

            mHandlers.add(handler);
            handler.startListening();
        } catch (IOException e) {
            e.printStackTrace();
            // TODO
            throw new Error("Не смогли создать хэндлер подумать как обрабтывать");
        }

    }
}
