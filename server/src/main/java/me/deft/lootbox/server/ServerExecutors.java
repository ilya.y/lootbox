package me.deft.lootbox.server;

import java.util.concurrent.Executor;

public class ServerExecutors {

    private Executor mNetworkExecutor;
    private Executor mIOExecutor;

    public ServerExecutors(Executor networkExecutor, Executor IOExecutor) {
        mNetworkExecutor = networkExecutor;
        mIOExecutor = IOExecutor;
    }

    public Executor net() {
        return mNetworkExecutor;
    }

    public Executor io() {
        return mIOExecutor;
    }

}
