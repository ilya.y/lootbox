package me.deft.lootbox.server.utils;

import me.deft.ServerConfig;
import me.deft.data.DirectoryInfo;
import me.deft.data.FileInfo;
import me.deft.data.FileType;
import me.deft.data.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.LinkedList;
import java.util.List;

public class FileUtils {

    private static final File ROOT = new File(ServerConfig.ROOT_STORAGE);
    private User mUser;

    public FileUtils(User user) {
        mUser = user;
    }

    public static void createMainStorage(){
        if (!ROOT.exists()) {
            ROOT.mkdir();
        }
    }

    /**
     * Возвращает содержимое указанного каталога для юзера
     * @param path путь относительно директории пользователя
     * @return описание каталога
     */
    public DirectoryInfo getDirectory(String path) {
        DirectoryInfo dir = new DirectoryInfo();
        List<FileInfo> filesList = new LinkedList<>();

        String userPath = getAbsoluteUserPath();
        String fullPath = userPath + path;

        File fileDir = new File(fullPath);
        File[] filesInDir = fileDir.listFiles();

        dir.setDirectoryName(fileDir.getName());
        dir.setPath(path);
        dir.setFiles(filesList);

        for (File f : filesInDir) {
            FileInfo fileInfo = new FileInfo.Builder()
                    .setFileName(f.getName())
                    .setPath(path)
                    .setType(f.isDirectory() ? FileType.DIRECTORY : FileType.FILE)
                    .setSize(f.length())
                    .build();
            filesList.add(fileInfo);
        }

        return dir;
    }

    public boolean createHomeDir() {
        File file = new File(getAbsoluteUserPath());
        if (file.exists())
            return true;

        file.setWritable(true);
        return file.mkdirs();
    }

    public boolean createDir(String path) {
        File file = new File(getAbsoluteUserPath() + path);
        if (!file.exists()) {
            return file.mkdirs();
        } else {
            return false;
        }
    }


    private String getAbsoluteUserPath() {
        return ROOT.getAbsolutePath() + File.separator + mUser.getLogin() + File.separator;
    }

    public FileOutputStream getFileOutputStream (FileInfo info) {
        try {
            return new FileOutputStream(getAbsoluteUserPath() + info.getUserFilePath());
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    public FileInputStream getFileInputStream (FileInfo info) {
        try {
            File f = new File(getAbsoluteUserPath() + info.getUserFilePath());
            return new FileInputStream(f);
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    public boolean removeFile(FileInfo info) {
        File f = new File(getAbsoluteUserPath() + info.getUserFilePath());
        if (f.exists())
            return f.delete();

        return false;
    }


}
