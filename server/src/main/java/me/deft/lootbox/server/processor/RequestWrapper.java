package me.deft.lootbox.server.processor;

import me.deft.lootbox.server.ClientSession;
import me.deft.messages.Request;
import me.deft.messages.Response;

/**
 * Created by iyakovlev on 18.09.17.
 */

public class RequestWrapper<T extends Request> {

    private T mRequest;
    private ClientSession mClientSession;

    public RequestWrapper(T request, ClientSession clientSession) {
        mRequest = request;
        mClientSession = clientSession;
    }

    public T getRequest() {
        return mRequest;
    }

    public ClientSession getClientSession() {
        return mClientSession;
    }
}
