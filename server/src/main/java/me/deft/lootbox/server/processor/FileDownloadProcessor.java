package me.deft.lootbox.server.processor;

import me.deft.data.ConnectionInfo;
import me.deft.data.FileInfo;
import me.deft.lootbox.server.LootboxServer;
import me.deft.lootbox.server.connection.FileTransmissionHandler;
import me.deft.lootbox.server.utils.Log;
import me.deft.messages.FileDownloadRequest;
import me.deft.messages.FileDownloadResponse;

/**
 * Created by iyakovlev on 21.09.17.
 */

public class FileDownloadProcessor extends AbstractProcessor<FileDownloadRequest, FileDownloadResponse> {

    @Override
    public void process(RequestWrapper<FileDownloadRequest> request, ProcessorListener<FileDownloadResponse> listener) {
        FileInfo fileInfo = request.getRequest().getFileInfo();
        FileDownloadResponse response = new FileDownloadResponse(request.getRequest());
        Log.log("Downloading file: " + fileInfo.getName());
        final ConnectionInfo info;
        try {
            info = LootboxServer.getInstance().getConnectionPool().get();
            response.setData(info);
            onSuccess(response, listener);
        } catch (InterruptedException e) {
            response.setError(FileDownloadResponse.ERROR_UPLOAD_FAILED);
            listener.onProcessed(response);
            return;
        }

        Log.log("Created connection info: port: " + info.getPort());
        new FileTransmissionHandler(
                fileInfo,
                request.getClientSession(),
                info
        ).send(FileTransmissionHandler.TIMEOUT_30_SECONDS, new FileTransmissionHandler.ResultListener() {
            @Override
            public void onFinished(boolean result) {
                LootboxServer.getInstance().getConnectionPool().recycle(info);
            }
        });
    }

    @Override
    public boolean needAuth() {
        return true;
    }
}
