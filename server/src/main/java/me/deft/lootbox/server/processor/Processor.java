package me.deft.lootbox.server.processor;

import me.deft.messages.Request;
import me.deft.messages.Response;

/**
 * Created by iyakovlev on 13.09.17.
 */

public interface Processor<T extends Request<R>, R extends Response> {

    interface ProcessorListener<R> {
        void onProcessed(R result);
    }

    void process(RequestWrapper<T> request, ProcessorListener<R> listener);

    /**
     * Говорит о том, должен ли запрос, который обрабатывается процессором, быть авторизован
     * @return true, если сессия должна быть авторизована
     */
    boolean needAuth();
}
