package me.deft.lootbox.server.connection;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import me.deft.ServerConfig;
import me.deft.data.ConnectionInfo;

/**
 * Created by iyakovlev on 21.09.17.
 */

public class ConnectionPool {

    BlockingQueue<ConnectionInfo> mConnections = new LinkedBlockingQueue<>();

    public ConnectionPool() {
        mConnections.add(new ConnectionInfo(ServerConfig.ADDRESS_PUBLIC, 14030));
        mConnections.add(new ConnectionInfo(ServerConfig.ADDRESS_PUBLIC, 14031));
        mConnections.add(new ConnectionInfo(ServerConfig.ADDRESS_PUBLIC, 14032));
        mConnections.add(new ConnectionInfo(ServerConfig.ADDRESS_PUBLIC, 14033));
        mConnections.add(new ConnectionInfo(ServerConfig.ADDRESS_PUBLIC, 14034));
        mConnections.add(new ConnectionInfo(ServerConfig.ADDRESS_PUBLIC, 14035));
        mConnections.add(new ConnectionInfo(ServerConfig.ADDRESS_PUBLIC, 14036));
        mConnections.add(new ConnectionInfo(ServerConfig.ADDRESS_PUBLIC, 14037));
        mConnections.add(new ConnectionInfo(ServerConfig.ADDRESS_PUBLIC, 14038));
        mConnections.add(new ConnectionInfo(ServerConfig.ADDRESS_PUBLIC, 14039));
    }

    public ConnectionInfo get() throws InterruptedException {
        return mConnections.take();
    }

    public void recycle(ConnectionInfo info) {
        mConnections.add(info);
    }
}
